package com.nicloay.aplatform.task.aabbtree;

import java.util.Vector;

/**
 * Created by
 * User: nicloay
 * Date: 15/05/14
 * Time: 23:06
 */
public class RaycastMesh implements NodeInterface {
    int      mRaycastFrame;
    int      mMaxNodeCount;
    int      mNodeCount;
    int      mVcount;
    int      mTcount;
    int   [] triangleRaycastFrameNumber;
    double[] mVertices;
    int   [] mIndices;

    NodeAABB[]      mNodes;
    NodeAABB        mRoot;
    Vector<Integer> mLeafTriangles;

    /**
     *
     * @param      vCount   The number of vertices in the source triangle mesh
     * @param    vertices   The array of vertex positions in the format x1,y1,z1..x2,y2,z2.. etc.
     * @param      tCount   The number of triangles in the source triangle mesh
     * @param    tIndices   The triangle indices in the format of i1,i2,i3 ... i4,i5,i6, ...
     * @param    maxDepth   Maximum recursion depth for the triangle mesh.
     * @param minLeafSize   minimum triangles to treat as a 'leaf' node.
     * @param minAxisSize   once a particular axis is less than this size, stop sub-dividing.
     */
    public RaycastMesh(int vCount, double[] vertices, int tCount, int[] tIndices,
                       int maxDepth, int minLeafSize, double minAxisSize) {
        mRaycastFrame = 0;
        maxDepth = Math.max(2, Math.min(16, maxDepth));
        int pow2Table[] = new int[]{ 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 65536 };
        mMaxNodeCount = 0;

        for (int i = 0; i < maxDepth ; i++) {
            mMaxNodeCount +=pow2Table[i];
        }

        mNodes     = new NodeAABB[mMaxNodeCount];
        mNodeCount = 0;

        mVcount   = vCount;
        mVertices = vertices;

        mTcount  = tCount;
        mIndices = tIndices;
        mLeafTriangles = new Vector<Integer>();

        triangleRaycastFrameNumber = new int[tCount];
        mRoot = new NodeAABB(mVcount, mVertices, mTcount, mIndices, maxDepth, minLeafSize, minAxisSize, this, mLeafTriangles);
    }

    @Override
    public NodeAABB getNode() {
        assert (mNodeCount < mMaxNodeCount);
        NodeAABB result = mNodes[mNodeCount] = new NodeAABB();
        mNodeCount++;
        return  result;
    }


    public boolean raycast(double[] from, double[] direction, HitInfo hitInfo)
    {
        hitInfo.clear();
        mRaycastFrame++;
        mRoot.raycast(from, direction, hitInfo, mVertices, mIndices, triangleRaycastFrameNumber, mRaycastFrame, mLeafTriangles);
        return hitInfo.isHit;
    }
}
