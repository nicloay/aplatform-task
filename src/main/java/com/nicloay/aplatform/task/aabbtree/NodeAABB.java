package com.nicloay.aplatform.task.aabbtree;

import org.apache.commons.lang.mutable.MutableDouble;
import org.apache.commons.lang.mutable.MutableInt;

import java.util.Vector;


/**
 * Created by
 * User: nicloay
 * Date: 15/05/14
 * Time: 23:04
 */
public class NodeAABB {
    public static final int TRI_EOF = -1;
    static final double RAYAABB_EPSILON = 0.00001f;

    NodeAABB   mLeft;
    NodeAABB   mRight;
    BoundsAABB mBounds;
    int        mLeafTriangleIndex;


    public NodeAABB(){
        mLeft              = null;
        mRight             = null;
        mLeafTriangleIndex = TRI_EOF;
    }

    public void resetNodeAABBBounds(BoundsAABB bounds){
        mBounds            = bounds;
        mLeft              = null;
        mRight             = null;
        mLeafTriangleIndex = TRI_EOF;
    }

    public NodeAABB(int vCount, double[] vertices, int tCount, int[] tIndices,
                    int maxDepth, int minLeafSize, double minAxisSize,
                    NodeInterface callback, Vector<Integer> leafTriangles){
        mLeft = null;
        mRight = null;
        mLeafTriangleIndex = TRI_EOF;
        Vector<Integer> triangles = new Vector<Integer>(tCount);
        for (int i = 0; i < tCount; i++) {
            triangles.add(i);
        }
        mBounds = new BoundsAABB();
        mBounds.setMin(vertices);
        mBounds.setMax(vertices);
        double x,y,z;
        int vertId = 0;
        for (int i = 0; i <vCount ; i++) {
            x = vertices[vertId++];
            y = vertices[vertId++];
            z = vertices[vertId++];

            mBounds.include(new double[]{x, y, z});
        }

        split(triangles, vertices, tIndices,
                0, maxDepth, minLeafSize, minAxisSize, callback, leafTriangles);
    }

    void split(Vector<Integer> triangles, double[] vertices, int[] indices,
                int depth, int maxDepth, int minLeafSize, double minAxisSize, NodeInterface callback, Vector<Integer> leafTriangles){
        double dx = mBounds.mMax[0] - mBounds.mMin[0];
        double dy = mBounds.mMax[1] - mBounds.mMin[1];
        double dz = mBounds.mMax[2] - mBounds.mMin[2];

        AxisAABB axis = AxisAABB.AABB_XAXIS;
        double lAxis = dx;

        if (dy > dx){
            axis = AxisAABB.AABB_YAXIS;
            lAxis = dz;
        }

        if (dz > dx && dz > dy){
            axis = AxisAABB.AABB_ZAXIS;
            lAxis = dz;
        }

        int count = triangles.size();


        // if the number of triangles is less than the minimum allowed for a leaf node or...
        // we have reached the maximum recursion depth or..
        // the width of the longest axis is less than the minimum axis size then...
        // we create the leaf node and copy the triangles into the leaf node triangle array.
        if (count < minLeafSize || depth >= maxDepth || lAxis < minAxisSize){
            mLeafTriangleIndex = leafTriangles.size();
            leafTriangles.add(count);
            for (Integer tri : triangles)
                leafTriangles.add(tri);
        } else {
            double[] center = new double[3];
            mBounds.getCenter(center);
            BoundsAABB b1 = new BoundsAABB();
            BoundsAABB b2 = new BoundsAABB();

            splitRect(axis, mBounds, b1, b2, center);

            BoundsAABB leftBounds = new BoundsAABB();
            BoundsAABB rightBounds = new BoundsAABB();

            Vector<Integer> leftTriangles = new Vector<Integer>();
            Vector<Integer> rightTriangles = new Vector<Integer>();

            for (Integer tri : triangles){
                int i1 = indices[tri * 3 + 0] * 3;
                int i2 = indices[tri * 3 + 1] * 3;
                int i3 = indices[tri * 3 + 2] * 3;

                double[] p1 = new double[]{vertices[i1], vertices[i1 + 1], vertices[i1 + 2]};
                double[] p2 = new double[]{vertices[i2], vertices[i2 + 1], vertices[i2 + 2]};
                double[] p3 = new double[]{vertices[i3], vertices[i3 + 1], vertices[i3 + 2]};

                int addCount = 0;
                MutableInt orCode = new MutableInt(0xFFFFFFFF); //the same as -1
                if (b1.containsTriangleExact(p1, p2, p3, orCode)){
                    addCount++;
                    if (leftTriangles.isEmpty()){
                        leftBounds.setMin(p1);
                        leftBounds.setMax(p1);
                    }
                    leftBounds.include(p1);
                    leftBounds.include(p2);
                    leftBounds.include(p3);
                    leftTriangles.add(tri);
                }
                // if the orCode is zero; meaning the triangle was fully self-contiained int he left bounding box; then we don't need to test against the right
                if (orCode.intValue() != -1 && b2.containsTriangleExact(p1, p2, p3, orCode)){
                    addCount++;
                    if (rightTriangles.isEmpty()){
                        rightBounds.setMin(p1);
                        rightBounds.setMax(p1);
                    }
                    rightBounds.include(p1);
                    rightBounds.include(p2);
                    rightBounds.include(p3);
                    rightTriangles.add(tri);
                }
                assert (addCount > 0);
            }

            if (!leftTriangles.isEmpty()){
                leftBounds.clamp(b1);
                mLeft = callback.getNode();
                mLeft.resetNodeAABBBounds(leftBounds);
                mLeft.split(leftTriangles, vertices, indices, depth + 1, maxDepth, minLeafSize, minAxisSize,callback, leafTriangles );
            }

            if (!rightTriangles.isEmpty()){
                rightBounds.clamp(b2);
                mRight = callback.getNode();
                mRight.resetNodeAABBBounds(rightBounds);
                mRight.split(rightTriangles, vertices, indices, depth + 1, maxDepth, minLeafSize, minAxisSize,callback, leafTriangles );
            }
        }
    }


    void splitRect(AxisAABB axis, BoundsAABB source, BoundsAABB b1, BoundsAABB b2, double[] midPoint){
        switch ( axis )
        {
            case AABB_XAXIS:
            {
                b1.setMin( source.mMin );
                b1.setMax( midPoint[0], source.mMax[1], source.mMax[2] );

                b2.setMin( midPoint[0], source.mMin[1], source.mMin[2] );
                b2.setMax(source.mMax);
            }
            break;
            case AABB_YAXIS:
            {
                b1.setMin(source.mMin);
                b1.setMax(source.mMax[0], midPoint[1], source.mMax[2]);

                b2.setMin(source.mMin[0], midPoint[1], source.mMin[2]);
                b2.setMax(source.mMax);
            }
            break;
            case AABB_ZAXIS:
            {
                b1.setMin(source.mMin);
                b1.setMax(source.mMax[0], source.mMax[1], midPoint[2]);

                b2.setMin(source.mMin[0], source.mMin[1], midPoint[2]);
                b2.setMax(source.mMax);
            }
            break;
        }
    }




    public void raycast(double[]        from,
                        double[]        dir,
                        HitInfo         hitInfo,
                        double[]        vertices,
                        int[]           indices,
                        int[]           raycastTriangles,
                        int             raycastFrame,
                        Vector<Integer> leafTriangles)
    {

        if ( !intersectRayAABB(mBounds.mMin, mBounds.mMax, from, dir) )
        {
            return;
        }

        if ( mLeafTriangleIndex != TRI_EOF )
        {

            int scan = mLeafTriangleIndex;
            int count = leafTriangles.get(scan++);
            for (int i=0; i<count; i++)
            {
                int tri = leafTriangles.get(scan++);
                if ( raycastTriangles[tri] != raycastFrame )
                {
                    raycastTriangles[tri] = raycastFrame;
                    int i1 = indices[tri*3+0] * 3;
                    int i2 = indices[tri*3+1] * 3;
                    int i3 = indices[tri*3+2] * 3;

                    double p1[] = new double[]{ vertices[i1],vertices[i1+1], vertices[i1+2]};
                    double p2[] = new double[]{ vertices[i2],vertices[i2+1], vertices[i2+2]};
                    double p3[] = new double[]{ vertices[i3],vertices[i3+1], vertices[i3+2]};


                    MutableDouble t = new MutableDouble(0);
                    if ( rayIntersectsTriangle(from,dir,p1,p2,p3,t))
                    {
                        boolean accept = false;
                        if ( t.doubleValue() == hitInfo.distance && tri < hitInfo.triangleId)
                        {
                            accept = true;
                        }
                        if ( t.doubleValue() < hitInfo.distance || accept )
                        {
                            hitInfo.distance = t.doubleValue();

                            hitInfo.triangleId = tri;
                            hitInfo.isHit = true;
                        }
                    }
                }
            }
        }
        else
        {
            if ( mLeft != null)
            {
                mLeft. raycast(from, dir, hitInfo, vertices, indices, raycastTriangles, raycastFrame, leafTriangles);
            }
            if ( mRight != null )
            {
                mRight.raycast(from, dir, hitInfo, vertices, indices, raycastTriangles, raycastFrame, leafTriangles);
            }
        }
    }

    static double[] coord = new double[3];  //don't create new instance every request and save a bit time for GC

    public static boolean intersectRayAABB(double[] MinB, double[] MaxB, double[] origin, double[] dir)
    {
        boolean Inside = true;
        double[] MaxT = new double[3];
        MaxT[0]=MaxT[1]=MaxT[2]=-1.0f;

        // Find candidate planes.
        for(int i=0;i<3;i++)
        {
            if(origin[i] < MinB[i])
            {
                coord[i]	= MinB[i];
                Inside		= false;

                // Calculate T distances to candidate planes
                if(dir[i] != 0.0f)
                    MaxT[i] = (MinB[i] - origin[i]) / dir[i];
            }
            else if(origin[i] > MaxB[i])
            {
                coord[i]	= MaxB[i];
                Inside		= false;

                // Calculate T distances to candidate planes
                if(dir[i] != 0.0f)
                    MaxT[i] = (MaxB[i] - origin[i]) / dir[i];
            }
        }

        // Ray origin inside bounding box
        if(Inside)
        {
            coord[0] = origin[0];
            coord[1] = origin[1];
            coord[2] = origin[2];
            return true;
        }

        // Get largest of the maxT's for final choice of intersection
        int WhichPlane = 0;
        if(MaxT[1] > MaxT[WhichPlane])
            WhichPlane = 1;
        if(MaxT[2] > MaxT[WhichPlane])
            WhichPlane = 2;

        // Check final candidate actually inside box
        if(MaxT[WhichPlane] < 0.0f)
            return false;

        for(int i=0;i<3;i++)
        {
            if(i!=WhichPlane)
            {
                coord[i] = origin[i] + MaxT[WhichPlane] * dir[i];
                if(coord[i] < MinB[i] - RAYAABB_EPSILON || coord[i] > MaxB[i] + RAYAABB_EPSILON)	return false;
                // NO EPSILON calc here
                //if(coord[i] < MinB[i] || coord[i] > MaxB[i])	return false;
            }
        }
        return true;	// ray hits box
    }


    static boolean rayIntersectsTriangle(double[] p, double[] d, double[] v0, double[] v1, double[] v2, MutableDouble t)
    {
        double[] e1 = new double[3];
        double[] e2 = new double[3];
        double[] h = new double[3];
        double[] s = new double[3];
        double[] q = new double[3];

        double a,f,u,v;

        vector(e1,v1,v0);
        vector(e2,v2,v0);
        crossProduct(h,d,e2);
        a = innerProduct(e1,h);

        if (a > -RAYAABB_EPSILON && a < RAYAABB_EPSILON)
            return(false);

        f = 1/a;
        vector(s,p,v0);
        u = f * (innerProduct(s,h));

        if (u < 0.0 || u > 1.0)
            return(false);

        crossProduct(q,s,e1);
        v = f * innerProduct(d,q);
        if (v < 0.0 || u + v > 1.0)
            return(false);
        // at this stage we can compute t to find out where
        // the intersection point is on the line
        t.setValue( f * innerProduct(e2,q));
        if (t.doubleValue() > 0) // ray intersection
            return true;
        else // this means that there is a line intersection
            // but not a ray intersection
            return false;
    }

    static void vector(double[] a, double[] b, double[] c){
        a[0] = b[0] - c[0];
        a[1] = b[1] - c[1];
        a[2] = b[2] - c[2];
    }

    static double innerProduct(double[] v, double[] q){
        return  v[0]*q[0] + v[1]*q[1] + v[2]*q[2];
    }

    static void crossProduct(double[] a, double[] b, double[] c){
        a[0] = b[1]*c[2] - c[1]*b[2];
        a[1] = b[2]*c[0] - c[2]*b[0];
        a[2] = b[0]*c[1] - c[0]*b[1];
    }
}
