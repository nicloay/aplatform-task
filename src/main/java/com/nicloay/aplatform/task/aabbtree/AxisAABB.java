package com.nicloay.aplatform.task.aabbtree;

/**
 * Created by
 * User: nicloay
 * Date: 16/05/14
 * Time: 09:30
 */
public enum AxisAABB {
    AABB_XAXIS,
    AABB_YAXIS,
    AABB_ZAXIS
}
