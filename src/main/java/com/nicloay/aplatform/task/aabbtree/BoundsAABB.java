package com.nicloay.aplatform.task.aabbtree;

import org.apache.commons.lang.mutable.MutableInt;

/**
 * Created by
 * User: nicloay
 * Date: 15/05/14
 * Time: 23:21
 */
public class BoundsAABB {
    public static Integer CC_MINX = 1<<0;
    public static Integer CC_MAXX = 1<<1;
    public static Integer CC_MINY = 1<<2;
    public static Integer CC_MAXY = 1<<3;
    public static Integer CC_MINZ = 1<<4;
    public static Integer CC_MAXZ = 1<<5;

    public double[] mMin;
    public double[] mMax;

    public BoundsAABB(){
        mMin = new double[3];
        mMax = new double[3];
    }


    public void setMin(double[] v){
        mMin[0] = v[0];
        mMin[1] = v[1];
        mMin[2] = v[2];
    }

    public void setMax(double[] v){
        mMax[0] = v[0];
        mMax[1] = v[1];
        mMax[2] = v[2];
    }

    public void setMin(double x, double y, double z){
        mMin[0] = x;
        mMin[1] = y;
        mMin[2] = z;
    }

    public void setMax(double x, double y, double z){
        mMax[0] = x;
        mMax[1] = y;
        mMax[2] = z;
    }

    public void include(double[] v){
        if ( v[0] < mMin[0] ) mMin[0] = v[0];
        if ( v[1] < mMin[1] ) mMin[1] = v[1];
        if ( v[2] < mMin[2] ) mMin[2] = v[2];

        if ( v[0] > mMax[0] ) mMax[0] = v[0];
        if ( v[1] > mMax[1] ) mMax[1] = v[1];
        if ( v[2] > mMax[2] ) mMax[2] = v[2];
    }

    public void getCenter(double[] center) {
        center[0] = (mMin[0] + mMax[0]) * 0.5f;
        center[1] = (mMin[1] + mMax[1]) * 0.5f;
        center[2] = (mMin[2] + mMax[2]) * 0.5f;
    }

    public boolean intersects(BoundsAABB b){
        if ((mMin[0] > b.mMax[0]) || (b.mMin[0] > mMax[0]))
            return false;
        if ((mMin[1] > b.mMax[1]) || (b.mMin[1] > mMax[1]))
            return false;
        if ((mMin[2] > b.mMax[2]) || (b.mMin[2] > mMax[2]))
            return false;
        return true;
    }

    boolean containsTriangle(double[] p1, double[] p2, double[] p3) {
        BoundsAABB b = new BoundsAABB();
        b.setMin(p1);
        b.setMax(p1);
        b.include(p2);
        b.include(p3);
        return intersects(b);
    }

    void clamp(BoundsAABB aabb){
        if ( mMin[0] < aabb.mMin[0] ) mMin[0] = aabb.mMin[0];
        if ( mMin[1] < aabb.mMin[1] ) mMin[1] = aabb.mMin[1];
        if ( mMin[2] < aabb.mMin[2] ) mMin[2] = aabb.mMin[2];
        if ( mMax[0] > aabb.mMax[0] ) mMax[0] = aabb.mMax[0];
        if ( mMax[1] > aabb.mMax[1] ) mMax[1] = aabb.mMax[1];
        if ( mMax[2] > aabb.mMax[2] ) mMax[2] = aabb.mMax[2];
    }



    boolean containsTriangleExact(double[] p1, double[] p2, double[] p3, MutableInt orCode){
        boolean ret = false;

        MutableInt andCode = new MutableInt();
        orCode.setValue(getClipCode(p1, p2, p3, andCode));
        if ( andCode.intValue() == 0 )
        {
            ret = true;
        }

        return ret;
    }


    Integer getClipCode(double[] p1, double[] p2, double[] p3, MutableInt andCodeMutable){
        int andCode = 0xFFFFFFFF;
        Integer c1 = getClipCode(p1);
        Integer c2 = getClipCode(p2);
        Integer c3 = getClipCode(p3);
        andCode &= c1;
        andCode &= c2;
        andCode &= c3;
        andCodeMutable.setValue(andCode);
        return c1|c2|c3;
    }

    Integer getClipCode(double[] p){
        Integer ret = 0;

        if ( p[0] < mMin[0] )
        {
            ret|=CC_MINX;
        }
        else if ( p[0] > mMax[0] )
        {
            ret|=CC_MAXX;
        }

        if ( p[1] < mMin[1] )
        {
            ret|=CC_MINY;
        }
        else if ( p[1] > mMax[1] )
        {
            ret|=CC_MAXY;
        }

        if ( p[2] < mMin[2] )
        {
            ret|=CC_MINZ;
        }
        else if ( p[2] > mMax[2] )
        {
            ret|=CC_MAXZ;
        }

        return ret;
    }
}
