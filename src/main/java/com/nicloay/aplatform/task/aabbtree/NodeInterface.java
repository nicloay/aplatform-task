package com.nicloay.aplatform.task.aabbtree;

/**
 * Created by
 * User: nicloay
 * Date: 15/05/14
 * Time: 23:03
 */
public interface NodeInterface {
    NodeAABB getNode();
}
