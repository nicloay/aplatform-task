package com.nicloay.aplatform.task.aabbtree;

/**
 * Created by
 * User: nicloay
 * Date: 20/05/14
 * Time: 14:45
 */
public class HitInfo {
    public double   distance;
    public int      triangleId;
    public boolean  isHit;

    public HitInfo() {
        distance    = Double.MAX_VALUE;
        isHit       = false;
        triangleId  = NodeAABB.TRI_EOF;
    }


    public void clear(){
        distance        = Double.MAX_VALUE;
        triangleId      = NodeAABB.TRI_EOF;
        isHit           = false;
    }

}
