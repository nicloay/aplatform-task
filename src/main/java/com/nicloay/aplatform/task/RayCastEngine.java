package com.nicloay.aplatform.task;

import com.nicloay.aplatform.task.aabbtree.HitInfo;
import com.nicloay.aplatform.task.aabbtree.RaycastMesh;
import com.nicloay.aplatform.task.model.Face;
import com.nicloay.aplatform.task.model.Object;
import com.nicloay.aplatform.task.model.Point;
import com.nicloay.aplatform.task.model.Scene;
import org.apache.commons.io.input.BOMInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;

/**
 * Created by
 * User: nicloay
 * Date: 06/05/14
 * Time: 16:00
 *
 *
 *   This code is based on raycastmesh written by John W. Ratcliff on C++ and released under MIT license.
 * source code available <a href="https://code.google.com/p/raycastmesh/">here</a>.
 * As you can find rycastmesh is based on <a href="http://www.codercorner.com/Opcode.html">OPCODE</a>
 *
 */

public class RayCastEngine {
    private final static int      MAX_DEPTH               = 20;
    private final static double   MIN_AXIS_SIZE           = 0.1f;
    private final static int     MIN_LEAF_SIZE           = 3;

    private final static boolean LOG_ENABLED             = false;
    private final static String  INPUT_DELIMITER_PATTERN = ";|,|\\n";
    public static void main(String[] args) {
        if (args.length == 0){
            System.err.println("no scene path provided");
            System.exit(1);
        }
        if (!LOG_ENABLED)
            java.util.logging.Logger.getLogger("").setLevel(Level.OFF);

        RayCastEngine engine = new RayCastEngine(args[0]);
        engine.startIoRead();
    }

    String scenePath;
    public RayCastEngine(String scenePath) {
        buildScene(scenePath);
    }



    private void startIoRead() {
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter(INPUT_DELIMITER_PATTERN);


        int requestId;
        double[] point     = new double[3];
        double[] direction = new double[3];
        HitInfo hitInfo    = new HitInfo();
        while(true){
            requestId    = scanner.nextInt();
            point[0]     = scanner.nextFloat();
            point[1]     = scanner.nextFloat();
            point[2]     = scanner.nextFloat();

            direction[0] = scanner.nextFloat();
            direction[1] = scanner.nextFloat();
            direction[2] = scanner.nextFloat();

            if(raycastMesh.raycast(point, direction, hitInfo)){
                System.out.println(requestId+":"+objectByTriId[hitInfo.triangleId]);
            } else {
                System.out.println(requestId+":");
            }
        }
    }

    public boolean rayCast(double px, double py, double pz, double dx, double dy, double dz, HitInfo hitInfo){
        return raycastMesh.raycast(new double[]{px,py,pz}, new double[]{dx,dy,dz}, hitInfo);
    }


    String[] objectByTriId;
    RaycastMesh raycastMesh;
    public void buildScene(String scenePath)  {
        this.scenePath = scenePath;
        Scene scene = deserializeSceneFromXML();
        HashMap<Integer, Point> pointById = new HashMap<Integer, Point>();
        for (Point p: scene.getPoints().getPoint())
            pointById.put(p.getId(), p);

        HashMap<Integer, Face>  faceById = new HashMap<Integer, Face>();
        for (Face f: scene.getFaces().getFace())
            faceById.put(f.getId(), f);


        int verticesCount = scene.getPoints().getPoint().size();
        double[] vertices= new double[verticesCount*3];
        int i=0;
        for (Point p:scene.getPoints().getPoint()){
            vertices[i++]=p.getX();
            vertices[i++]=p.getY();
            vertices[i++]=p.getZ();
        }

        int triangleCount = scene.getFaces().getFace().size();
        int[] triangles = new int[triangleCount*3];
        i=0;
        for (Face f: scene.getFaces().getFace()) {
            triangles[i++] =f.getPointId().get(0) - 1; //-1 is because source data based on 1
            triangles[i++] =f.getPointId().get(1) - 1;
            triangles[i++] =f.getPointId().get(2) - 1;
        }

        objectByTriId =  new String[triangleCount];
        for (Object o: scene.getObjects().getObject()){
            for (Integer faceId: o.getFaceId())
                objectByTriId[faceId - 1]=o.getName(); //-1 is because source data based on 1
        }


        raycastMesh = new RaycastMesh(verticesCount,vertices, triangleCount, triangles, MAX_DEPTH, MIN_LEAF_SIZE, MIN_AXIS_SIZE);

    }

    private Scene deserializeSceneFromXML() {
        try {
            JAXBContext context = JAXBContext.newInstance(Scene.class);
            Unmarshaller m = context.createUnmarshaller();

            BOMInputStream bs = new BOMInputStream(new FileInputStream(scenePath));
            return (Scene)m.unmarshal(bs);

        } catch (JAXBException e) {
            System.err.println("problem with scene deserialization");
            e.printStackTrace();
            System.exit(7);
        }  catch (FileNotFoundException e) {
            System.err.println("Scene file not found");
            e.printStackTrace();
            System.exit(5);
        }
        return  null;
    }

}
