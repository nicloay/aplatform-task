package com.nicloay.aplatform.task;

import com.nicloay.aplatform.task.aabbtree.HitInfo;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;

import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertFalse;

public class RayCastEngineTest {

    public final static String SCENE_FILE_NAME="/example.xml";
    RayCastEngine engine;
    HitInfo hitInfo = new HitInfo();


    @BeforeTest
    public void startService() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        String path = getClass().getResource(SCENE_FILE_NAME).getPath();
        engine = new RayCastEngine(path);
    }

    @Test
    public void testPredefinedString1(){
        engine.rayCast(1.0d, -0.5d, 5.0d, 0.0d, 0.0d, -1.0d, hitInfo);
        assertTrue(hitInfo.isHit);
    }

    @Test
    public void testPredefinedString2() {
        assertFalse(engine.rayCast(1.0d, 1.0d, 1.0d, 1.0d, 1.0d, 1.0d, hitInfo));
    }

    @Test
    public void testPredefinedString3() {
        assertTrue(engine.rayCast(-1.0d, -0.5d, 5.0d, 0.0d, 0.0d, -100.0d, hitInfo));
    }

    @Test
    public void testPredefinedString4(){
        assertTrue (engine.rayCast( 0.0d,  0.0d, 0.0d, 220.0d, 220.0d,  524.0d, hitInfo));
    }

}