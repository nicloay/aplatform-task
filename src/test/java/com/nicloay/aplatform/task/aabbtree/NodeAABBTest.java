package com.nicloay.aplatform.task.aabbtree;

import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class NodeAABBTest {

    @Test
    public void testIntersectRayAABBIn() throws Exception {
        double[] minB = new double[]{-1,-1,-1};
        double[] maxB = new double[]{1, 1, 1};
        double[] origin = new double[]{-2,-2,-2};
        double[] direction = new double[]{0.002f,0.002f,0.002f};


        boolean result = NodeAABB.intersectRayAABB(minB, maxB, origin, direction);
        assertTrue(result, "ray should intersect bounds");
    }

    @Test
    public void testIntersectRayAABBOut() throws Exception {
        double[] minB      = new double[]{ -1, -1, -1};
        double[] maxB      = new double[]{  1,  1,  1};
        double[] origin    = new double[]{ -2, -2, -2};
        double[] direction = new double[]{-0.01f,-0.01f,-0.01f};

        boolean result = NodeAABB.intersectRayAABB(minB, maxB, origin, direction);
        assertFalse(result, "ray should not intersecting bounds");
    }


    @Test
    public void testIntersectRayAABBWithin() throws Exception {
        double[] minB      = new double[]{ -1, -1, -1};
        double[] maxB      = new double[]{  1,  1,  1};
        double[] origin    = new double[]{  0,  0,  0};
        double[] direction = new double[]{-21,-21,-21};

        boolean result = NodeAABB.intersectRayAABB(minB, maxB, origin, direction);
        assertTrue(result, "ray should intersecting bounds");
    }

}